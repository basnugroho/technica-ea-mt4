//+------------------------------------------------------------------+
//|                                                  Tugas Akhir.mq4 |
//|                                   Copyright 2017, sekolahtrading |
//|                                   https://www.jurutrading.com    |
//+------------------------------------------------------------------+

#include <stdlib.mqh>
#include <BaskoroLibrary\Markets.mqh>
#include <BaskoroLibrary\Prints.mqh>

#property copyright "Copyright 2017, sekolahtrading"
#property link      "https://www.jurutrading.com"
#property version   "1.05"
#property strict
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum EA_MODE
  {
   SEMI_AUTO,//Semi Auto Mode
   AUTO //Auto Mode
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum FIBO_RETC_LEVEL
  {
   LEVEL_0,//Level 0
   LEVEL_236, //Level 23.6
   LEVEL_382, //Level 38.2
   LEVEL_500, //Level 50
   LEVEL_618, //Level 61.8
   LEVEL_1000, //Level 100
   LEVEL_1270, //Level 127 Expansion
   LEVEL_1618 //Level 161.8 Expansion
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum TREND
  {
   UP,//Up Trend
   DOWN //Down Trend
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum NOTIFICATIONS
  {
   NONE,//Don't send notification
   EMAIL,//Send Email
   MOBILE_PHONE,//Send Mobile Notification
   EMAIL_AND_PHONE,//Send Email and Mobile Notification
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum AVERAGING_MODE
  {
   NO_AVERAGING,//Don't Average
   AVERAGING_DOWN,//Averaging Down
                  //AVERAGING_UP,//Averaging Up
   //AVERAING_UP_AND_DOWN,//Averaging Up and Down
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum INCREMENT_TYPE
  {
   STATIC, //Static
   LINEAR, //Linear
   MARTINGLE, //Martingle
   FIBONACCI, //Fibonacci
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum RISK_HANDLING
  {
   MANUAL,//Manual
   SMARTCUTLOSS,//Smart Stop Loss
   HEDGING,//Hedging
  };
//+------------------------------------------------------------------+
//|Input Parameters                                                  |
//+------------------------------------------------------------------+
extern string _="+-----------------------------------------------+";//GLOBAL PARAMETERS:
extern string inputRisk="1% Risk"; //Risk
NOTIFICATIONS sendNotification=NONE;
extern string emailTo=""; //Send Email
extern EA_MODE inputEA_MODE=SEMI_AUTO; //EA Mode
extern bool inputExitTimeFrame=false;//Use Reversal Signal
extern bool useTrailingStop=false;//Use Trailing Stop
extern string minimumTakeProfit=""; //Minimum Take Profit
extern int expert_id=1; //Expert ID
extern string inputComment=""; //Comment
extern string autoParams="+-----------------------------------------------+";//AUTO MODE PARAMETERS:
extern FIBO_RETC_LEVEL fiboLevelEntry=LEVEL_236; //Fibonacci Entry Level
extern FIBO_RETC_LEVEL fiboLevelSL=LEVEL_1000; //Fibonacci Stop Loss Level
extern FIBO_RETC_LEVEL fiboLevelTP=LEVEL_0; //Fibonacci Take Profit Level
extern bool orderFiboArea=true;//Open Order Inside Entry Area
extern bool trendChangeStop=true;//Stop at Trend Change
extern ENUM_TIMEFRAMES inputTimeFrame=PERIOD_H4; //Fibonacci Period
extern int shift=4; //Start Index
extern int to=5; //Last Index
extern string semiAutoParams="+-----------------------------------------------+"; //SEMI AUTO MODE PARAMETERS:
extern bool autoDrawSLTP=true; //Draw SLTP Lines
extern bool autoAdjustSLTP=false; //Auto Adjust SLTP Lines
extern bool allowBuy=true; //Allow Buy Order
extern bool allowSell=true; //Allow Sell Order
extern bool allowOppositeOrder=false; //Allow Opposite Order (Buy & Sell Together)
extern string avgParams="+-----------------------------------------------+"; //EXTENDED PARAMETERS:
extern AVERAGING_MODE averagingMode=NO_AVERAGING; //Averaging Mode
extern RISK_HANDLING inputRiskHandle=MANUAL;//Risk Handling
extern int maxOrders=8; //Maximum Orders
extern double orderDistance=30; //Order Distance
extern INCREMENT_TYPE incrementType=STATIC; //Lot Increment Type
extern string _____="-------------------------------------------------+";//--------------------------------------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|Global Varaibels                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
string eaName="Technica 1.05";
string website="http://bendamaya.com/test/cek";
string webmail="http://bendamaya.com/sendemail.php";
string EA_ID="1";
bool openwebif_fail=false;
string fail_website="http://bendamaya.com";
//--
string printTimeFrame;
string recognizedLines[]=
  {
   "Buy","Buy SL","Buy TP","Sell","Sell SL","Sell TP",
   "Buy Upper Band","Buy Lower Band","Sell Upper Band","Sell Lower Band",
   "Fibo Buy","Fibo Sell","Fibo Buy TP","Fibo Buy SL","Fibo Sell TP","Fibo Sell SL","Fibo Buy OP","Fibo Sell OP"
  };
int arrowObjects[]={OBJ_ARROW_UP,OBJ_ARROW_DOWN,OBJ_ARROW_STOP,OBJ_ARROW_CHECK,OBJ_ARROW_THUMB_DOWN,OBJ_ARROW_THUMB_UP};
TREND gTrend;
int gBuyTicket,gSellTicket,gFiboBuyTicket,gFiboSellTicket;
double buy_op,buy_tp,buy_sl,sell_op,sell_tp,sell_sl,buyLot,sellLot,stopLevel; //price
double stoplossBuy,stoplossSell; //pips
double gBuyRiskAmount,gSellRiskAmount,gBuyBreakEven,gSellBreakEven,gHedgingBreakEven; //price
int gBuyTotal,gSellTotal;
int minOppositeOrder,minBreakEven;
bool exitTimeframe;
double atr,adr;
double lockRatio=1.10;
double tp,sl,pla,plv,trailing,ts,minProfit;
int P=1;
bool isYenPair,isCryptoPair,isCommodity;
double macdMain,macdSignal,ma14,smartCloseBuy,smartCloseSell;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
//TREND tren = readTrend(
int OnInit()
  {
//--cek login
//if(!login())return(INIT_FAILED);
/*
   if(kirimEmail(emailTo,"Technica Activated","Yoeufwieu")==true)
     {
      Alert("Email Send");
     }
   else
     {
      Alert(kirimEmail(emailTo,"Technica Activated","Yoeufwieu"));
     }*/
//--cek digits
   if(Digits==5 || Digits==3 || Digits==1)P=10;else P=1; // To account for 5 digit brokers
   if(Digits==3 || Digits==2) isYenPair=true;
//--check Risk input
   if(hitungLot(inputRisk,0)==-1)
     {
      Comment("Wrong risk format!");
      return INIT_FAILED;
     }
   if(getMinimumTP2USD(minimumTakeProfit)==-1)
     {
      Comment("Wrong minim take profit format");
      return INIT_FAILED;
     }
   if(expert_id<0)
     {
      Comment("Expert ID must be greater than 0");
      return INIT_FAILED;
     }
   if(shift<0)
     {
      Comment("Start index must be greater than 0");
      return INIT_FAILED;
     }
   if(to<shift)
     {
      Comment("Last index must be greater than start index");
      return INIT_FAILED;
     }
   if(maxOrders<0)
     {
      Comment("Maximum order must be greater than 0");
      return INIT_FAILED;
     }
   if(orderDistance<0)
     {
      Comment("Order distance must be greater than 0");
      return INIT_FAILED;
     }
//--macd
   exitTimeframe=inputExitTimeFrame;
   macdMain=iMACD(Symbol(),PERIOD_CURRENT,12,26,9,PRICE_CLOSE,MODE_MAIN,0);//histogram
   macdSignal=iMACD(Symbol(),PERIOD_CURRENT,12,26,9,PRICE_CLOSE,MODE_SIGNAL,0);//ma
   ma14=iMA(NULL,0,14,0,MODE_SMA,PRICE_CLOSE,1);
   minBreakEven=2;
//--range pla/plv/trailing/ts
   atr=NormalizeDouble(iATR(Symbol(),PERIOD_CURRENT,14,0)/Point,2);
   adr=NormalizeDouble(iATR(Symbol(),PERIOD_D1,MathMin(64,iBars(Symbol(),PERIOD_D1)),1)/Point,2);
   pla= 5 * atr; plv= 0.1*pla; tp=0; sl=0;
   stopLevel=(MarketInfo(Symbol(),MODE_STOPLEVEL)+MarketInfo(Symbol(),MODE_SPREAD))/P; // Defining minimum StopLevel                                                  
   if(inputEA_MODE==SEMI_AUTO)//Create Button
     {
      ButtonCreate(0,"btnBuy",0,12,30,75,20,CORNER_LEFT_LOWER,"Buy 0.01","Arial",8,clrWhite,clrGreen,clrNONE,false,true,false,true,0);
      ButtonCreate(0,"btnSell",0,90,30,75,20,CORNER_LEFT_LOWER,"Sell 0.01","Arial",8,clrWhite,clrRed,clrNONE,false,true,false,true,0);
     }
//--Line prices
   buy_sl=getLinePrice("Buy SL");
   sell_sl=getLinePrice("Sell SL");
   stoplossBuy=getSLPips(Bid,buy_sl,P);
   stoplossSell=getSLPips(Ask,sell_sl,P);
//--order lots
   if(ObjectFind("Buy SL")<0)stoplossBuy=0;
   if(ObjectFind("Sell SL")<0)stoplossSell=0;
   gBuyTotal=getSpecifiedTotalsOrders(OP_BUY,expert_id);
   gSellTotal=getSpecifiedTotalsOrders(OP_SELL,expert_id);
   buyLot=NormalizeDouble(hitungLot(inputRisk,stoplossBuy),2);
   sellLot=NormalizeDouble(hitungLot(inputRisk,stoplossSell),2);
//--find recognized objects, give style, draw sltp
   for(int i=0;i<ArraySize(recognizedLines);i++)
     {
      if(ObjectFind(0,recognizedLines[i])>=0)
        {
         SetLineStyle(recognizedLines[i]);
         if(autoDrawSLTP>0)
           {
            if(ObjectFind(0,"Buy")>=0)
              {
               drawSLTP("Buy",autoAdjustSLTP);
              }
            if(ObjectFind(0,"Sell"))
              {
               drawSLTP("Sell",autoAdjustSLTP);
              }
           }
        }
     }
   setRiskInfo(inputRisk);
//show info
   if(averagingMode!=NO_AVERAGING && inputEA_MODE==SEMI_AUTO)showAveragingInfo();
   if(averagingMode==NO_AVERAGING && inputEA_MODE==SEMI_AUTO)showSemiAutoInfo();
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   ButtonDelete(0,"btnBuy");
   ButtonDelete(0,"btnSell");
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnTick()
  {
//cleaning
   clearAvgLines(expert_id);
//--macd
   macdMain=iMACD(Symbol(),PERIOD_CURRENT,12,26,9,PRICE_CLOSE,MODE_MAIN,0);
   macdSignal=iMACD(Symbol(),PERIOD_CURRENT,12,26,9,PRICE_CLOSE,MODE_SIGNAL,0);
   ma14=iMA(NULL,0,14,0,MODE_SMA,PRICE_CLOSE,1);
//--range pla/plv/trailing/ts
   atr=NormalizeDouble(iATR(Symbol(),PERIOD_CURRENT,14,0)/Point,2);
   adr=NormalizeDouble(iATR(Symbol(),PERIOD_D1,MathMin(64,iBars(Symbol(),PERIOD_D1)),1)/Point,2);
   pla= 5 * atr; plv=0.1*pla; tp=0; sl=0;
   stopLevel=(MarketInfo(Symbol(),MODE_STOPLEVEL)+MarketInfo(Symbol(),MODE_SPREAD))/P;
//--Line Prices                                                                                     //--
   buy_sl=getLinePrice("Buy SL");
   sell_sl=getLinePrice("Sell SL");
   buy_tp=getLinePrice("Buy TP");
   sell_tp=getLinePrice("Sell TP");
//--Pips Distances   
   stoplossBuy=getSLPips(Bid,buy_sl,P);
   stoplossSell=getSLPips(Ask,sell_sl,P);
//--order lots
   if(ObjectFind("Buy SL")<0)stoplossBuy=0;
   if(ObjectFind("Sell SL")<0)stoplossSell=0;
   buyLot=NormalizeDouble(hitungLot(inputRisk,stoplossBuy),2);
   sellLot=NormalizeDouble(hitungLot(inputRisk,stoplossSell),2);
//give style to recognized lines if exist
   for(int i=0;i<ArraySize(recognizedLines);i++)
     {
      if(ObjectFind(0,recognizedLines[i])>=0)
        {
         SetLineStyle(recognizedLines[i]);
         if(autoDrawSLTP>0) drawSLTP(recognizedLines[i],autoAdjustSLTP);
         //TREND LINE
         if(inputEA_MODE==SEMI_AUTO)
           {
            //OP by Chartist Lines
            if(i<3)gBuyTicket=isChartistLineTouched(recognizedLines[i],1,buyLot);
            if(i>=3)gSellTicket=isChartistLineTouched(recognizedLines[i],1,sellLot);
            //averaging by chartlist lines
            checkAveragingLine();
           }
        }
     }
//Risk Amount On SL Line
   setRiskInfo(inputRisk);
//Semi Auto
   if(inputEA_MODE==SEMI_AUTO)
     {
      //redraw breakeven
      //redrawBreakEven(OP_BUY);
      //redrawBreakEven(OP_SELL);
      //Trailing Stop (Semi Mode)
      if(getOrderProfitTotal(OP_BUY,expert_id)>getMinimumTP2USD(minimumTakeProfit))
        {
         if(useTrailingStop)trailingStop(pla,plv,30,Symbol(),OP_BUY,expert_id);
        }
      if(getOrderProfitTotal(OP_SELL,expert_id)>getMinimumTP2USD(minimumTakeProfit))
        {
         if(useTrailingStop)trailingStop(pla,plv,30,Symbol(),OP_SELL,expert_id);
        }
      //--OP by Arrow Objects
      checkObjectArrowOrders();
      //--button set
      ButtonTextChange(0,"btnBuy","Buy "+string(buyLot));
      ButtonTextChange(0,"btnSell","Sell "+string(sellLot));
     }
   if(averagingMode==AVERAGING_DOWN && inputEA_MODE!=AUTO)
     {
      //--Opening Buy/Sell Limit
      if(getSpecifiedTotalsOrders(OP_BUY,expert_id)<maxOrders)
        {
         if(getSpecifiedTotalsOrders(OP_BUYLIMIT,expert_id)<1 && getSpecifiedTotalsOrders(OP_BUY,expert_id)>0 && getOrderProfitTotal(OP_BUY,expert_id)<0)
           {
            int buyLimitTicket=OrderSend(Symbol(),OP_BUYLIMIT,incrementLot(OP_BUY,incrementType,expert_id),Ask-orderDistance*Point*P,5,0,0,inputComment,expert_id,0,clrBlue);
            if(buyLimitTicket>0)
              {
               redrawBreakEven(OP_BUY);
              }
            //--LOCK
/*
            if(getSpecifiedTotalsOrders(OP_BUY,expert_id)==maxOrders && getOrderProfitTotal(OP_BUY,expert_id)<=0)
              {
               if(inputRiskHandle==LOCK)
                 {
                  int lock=OrderSend(Symbol(),OP_SELL,getOrderLotsTotal(OP_BUY),Bid,50,0,0,inputComment,expert_id,0,clrBrown);
                  exitTimeframe=-1;
                 }
              }
              */
           }
        }
      if(getSpecifiedTotalsOrders(OP_SELL,expert_id)<maxOrders)
        {
         if(getSpecifiedTotalsOrders(OP_SELLLIMIT,expert_id)<1 && getSpecifiedTotalsOrders(OP_SELL,expert_id)>0 && getOrderProfitTotal(OP_SELL,expert_id)<0)
           {
            int sellLimitTicket=OrderSend(Symbol(),OP_SELLLIMIT,incrementLot(OP_SELL,incrementType,expert_id),Bid+orderDistance*Point*P,5,0,0,inputComment,expert_id,0,clrRed);
            if(sellLimitTicket>0)
              {
               redrawBreakEven(OP_SELL);
              }
            //--LOCK
/*
            if(getSpecifiedTotalsOrders(OP_SELL,expert_id)==maxOrders && getOrderProfitTotal(OP_SELL,expert_id)<=0)
              {
               if(inputRiskHandle==LOCK)
                 {
                  int lock=OrderSend(Symbol(),OP_BUY,getOrderLotsTotal(OP_SELL),Ask,50,0,0,inputComment,expert_id,0,clrBrown);
                  exitTimeframe=-1;
                 }
              }
              */
           }
        }

     }
//SMARCUTLOSS MA 14.2
   smartCloseBuy=ma14+(2*atr*Point);
   smartCloseSell=ma14-(atr*Point);
   if(getSpecifiedTotalsOrders(OP_BUY,expert_id)>=maxOrders && inputRiskHandle==SMARTCUTLOSS && getOrderProfitTotal(OP_BUY,expert_id)<0)
     {
      if(Bid>=smartCloseBuy)
        {
         int retClose=closeAllBuyOP();
         if(emailTo!="" && retClose==1)
           {
            kirimEmail(emailTo,"Buy Closed",Symbol()+" Buy Order Closed by Smart Stop Loss");
           }
        }
     }
   if(getSpecifiedTotalsOrders(OP_SELL,expert_id)>=maxOrders && inputRiskHandle==SMARTCUTLOSS && getOrderProfitTotal(OP_SELL,expert_id)<0)
     {
      if(Ask<=smartCloseSell)
        {
         int retClose=closeAllSellOP();
         if(emailTo!="" && retClose==1)
           {
            kirimEmail(emailTo,"Sell Closed",Symbol()+" Sell Order Closed by Smart Stop Loss");
           }
        }
     }
//HEDGING
   if(getSpecifiedTotalsOrders(OP_BUY,expert_id)==maxOrders && getOrderProfitTotal(OP_BUY,expert_id)<=0
      && inputRiskHandle==HEDGING)
     {
      int opHedging=0;
      if(getSpecifiedTotalsOrders(OP_SELL,expert_id)<1)
        {opHedging=opSell();}
      if(emailTo!="" && opHedging>0)
        {
         kirimEmail(emailTo,"Hedging Activated","Buy Order Opened Agains "
                    +(string)getSpecifiedTotalsOrders(OP_SELL,expert_id)+" Sell Position");
        }
     }
   if(getSpecifiedTotalsOrders(OP_SELL,expert_id)==maxOrders && getOrderProfitTotal(OP_SELL,expert_id)<=0
      && inputRiskHandle==HEDGING)
     {
      int opHedging=0;
      if(getSpecifiedTotalsOrders(OP_BUY,expert_id)<1) opSell();
      if(emailTo!="" && opHedging>0)
        {
         kirimEmail(emailTo,"Hedging Activated","Sell Order Opened Agains "
                    +(string)getSpecifiedTotalsOrders(OP_BUY,expert_id)+" Buy Position");
        }
     }

//MACD ENTRY
   if(ObjectFind("Buy Lower Band")>=0 && ObjectFind("Buy Upper Band")>=0 && getSpecifiedTotalsOrders(OP_BUY,expert_id)<1)
     {
      if(macdCross(macdMain,macdSignal)==1//cross up
         && getLinePrice("Buy Lower Band")<=Ask && Ask<=getLinePrice("Buy Upper Band"))
        {
         int op_macd= opBuy();
         if(emailTo!="" && op_macd>0)
           {
            kirimEmail(emailTo," Open By Reversal Signal",Symbol()+" Buy Order inside area opened");
           }
        }
     }
   if(ObjectFind("Sell Lower Band")>=0 && ObjectFind("Sell Upper Band")>=0 && getSpecifiedTotalsOrders(OP_SELL,expert_id)<1)
     {
      if(macdCross(macdMain,macdSignal)==2//cross up 
         && getLinePrice("Sell Lower Band")<=Bid && Bid<=getLinePrice("Sell Upper Band"))
        {
         int op_macd= opSell();
         if(emailTo!="" && op_macd>0)
           {
            kirimEmail(emailTo," Open By Reversal Signal",Symbol()+" Sell Order inside area opened");
           }
        }
     }
//MACD TP
   if(exitTimeframe && getOrderProfitTotal(OP_BUY,expert_id)>=getMinimumTP2USD(minimumTakeProfit)
      && macdCross(macdMain,macdSignal)==2) //(current)
     {
      int close_macd=closeAllBuyOP();
      if(emailTo!="" && close_macd>0)
        {
         kirimEmail(emailTo," Close By Reversal Signal",Symbol()+" Buy Order closed by reversal");
        }
     }
   if(exitTimeframe && getOrderProfitTotal(OP_SELL,expert_id)>=getMinimumTP2USD(minimumTakeProfit)
      && macdCross(macdMain,macdSignal)==1) //macdMain>macdSignal (current)
     {
      int close_macd=closeAllSellOP();
      if(emailTo!="" && close_macd>0)
        {
         kirimEmail(emailTo," Close By Reversal Signal",Symbol()+"Sell Order closed by reversal");
        }
     }
//--
   if(averagingMode!=NO_AVERAGING && inputEA_MODE==SEMI_AUTO)showAveragingInfo();//semi auto + extension
   if(averagingMode==NO_AVERAGING && inputEA_MODE==SEMI_AUTO)showSemiAutoInfo();//semi auto info
//+------------------------------------------------------------------------------------------------------------------+
//--AUTO MODE
   if(inputEA_MODE==AUTO)
     {
      gTrend=readTrend(inputTimeFrame,shift,to);
      //ObjectDelete("btnBuy");ObjectDelete("btnSell");
      gTrend=readTrend(inputTimeFrame,shift,to);
      if(gTrend==UP)
        {
         //gFiboBuyTicket=
         fiboAutoSignal(gFiboBuyTicket,gTrend,inputRisk,5,fiboLevelEntry,fiboLevelSL,fiboLevelTP,inputTimeFrame,shift,to);
        }
      if(gTrend==DOWN)
        {
         //gFiboSellTicket=
         fiboAutoSignal(gFiboSellTicket,gTrend,inputRisk,5,fiboLevelEntry,fiboLevelSL,fiboLevelTP,inputTimeFrame,shift,to);
        }
      //setTrailingStopAuto(OP_SELL,expert_id,plv);
      if(useTrailingStop && getOrderProfitTotal(OP_BUY,expert_id)>getMinimumTP2USD(minimumTakeProfit))
        {
         //Print("Fibo Sell Ticket: "+string(gFiboSellTicket));
         trailingStopAuto(pla,plv,atr,gFiboBuyTicket);
         //trailingStopFibo(pla,plv,30,Symbol(),OP_BUY,expert_id);
        }
      if(useTrailingStop && getOrderProfitTotal(OP_SELL,expert_id)>getMinimumTP2USD(minimumTakeProfit))
        {
         //Print("Fibo Sell Ticket: "+string(gFiboSellTicket));
         trailingStopAuto(pla,plv,atr,gFiboSellTicket);
         //trailingStopFibo(pla,plv,30,Symbol(),OP_SELL,expert_id);
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void showSemiAutoInfo()
  {
   Comment(
           eaName+", Last Modify: "+string(__DATETIME__)+", Expert ID: "+string(expert_id)+"/"+inputComment+
           "\n"+
           "\nLocal Time: "+TimeToStr(TimeLocal())+", GMT: "+TimeToStr(TimeGMT())+
           "\n"+
           "\nMinimun Profit: "+(string)getMinimumTP2USD(minimumTakeProfit)+" "+AccountCurrency()+
           "\n"+
           "\nBuy - Lots: "+string(buyLot)+", SL: "+string(NormalizeDouble(stoplossBuy*P,2))+", Risk: "+(string)NormalizeDouble(getAmountByPips(stoplossBuy,buyLot),2)+" "+AccountCurrency()+
           "\nSell - Lots: "+string(sellLot)+", SL: "+string(NormalizeDouble(stoplossSell*P,2))+", Risk: "+(string)NormalizeDouble(getAmountByPips(stoplossSell,sellLot),2)+" "+AccountCurrency()+
           "\n"+
           "\nATR: "+string(atr)+", Daily Range: "+string(adr)+"; SL/TP: "+"0.00/0.00"+
           "\n"+
           "\nEA Mode: "+EnumToString(inputEA_MODE)+

           "\nBuy :: Order: "+string(getSpecifiedTotalsOrders(OP_BUY,expert_id))+"; Profit: "+(string)getOrderProfitTotal(OP_BUY,expert_id)+"; Swap: $"+doubleToStrKurung(swaplong)+"/lot"
           "\nSell :: Order: "+string(getSpecifiedTotalsOrders(OP_SELL,expert_id))+"; Profit: "+(string)getOrderProfitTotal(OP_SELL,expert_id)+"; Swap: $"+doubleToStrKurung(swapshort)+"/lot"
           "\n"+printTotalProfit(Symbol(),expert_id)+" "+printProfitBalanceRatio()
           );
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void showAveragingInfo()
  {
   Comment(
           eaName+", Last Modify: "+string(__DATETIME__)+", Expert ID: "+string(expert_id)+"/"+inputComment+
           "\n"+
           "\nLocal Time: "+TimeToStr(TimeLocal())+", GMT: "+TimeToStr(TimeGMT())+
           "\n"+
           "\nLimit Buy: "+(string)(orderDistance*P)+", Limit Sell: "+(string)(orderDistance*P)+
           ", Treshold: "+(string)(stopLevel*Point*P)+
           ", Buy SL: "+(string)(stoplossBuy*P)+", Sell SL: "+(string)sell_sl+
           ", Buy TP: "+", Sell TP: "+
           "\nMinimun Profit: "+(string)getMinimumTP2USD(minimumTakeProfit)+" "+AccountCurrency()+
           ", ProfitLock: "+(string)(2*adr)+"/"+(string)(0.2*adr)+
           "\n"+
           "\n"+"***Risk: Maximum calculated risk based on setting is around $"+
           (string)getAmountByPips(adr*P,maxOrders*hitungLot(inputRisk,sl))+
           ", lots: "+string(maxOrders*hitungLot(inputRisk,sl))+
           ", Expected Retracement "+printExpectedRetracement(incrementType,maxOrders)+"***"+
           "\n ***WARNING: Your risk is variable, depend on the Smart Stop Loss/Lock method, it can be as big as your account***"+
           "\n"+
           "\nRisk Method: "+EnumToString(inputRiskHandle)/*+" - Approximate "*/+
           //"\nFloating: $"+getOrderProfitTotal(OP_BUY,0)+getOrderProfitTotal(OP_SELL,0)+" ("
           //+MarketInfo(Symbol(),MODE/AccountBalance()*100+"%) "+
           //"Max: $"+AccountBalance()+
           "\nLot Increment: "+EnumToString(incrementType)+", Max Orders: "+(string)maxOrders+
           "\nUse Reversal Signal: "+(string)inputExitTimeFrame+
           "\nAfter "+(string)minBreakEven+" opened, order will be closed at breakeven"+
           "\nATR: "+string(atr)+", Daily Range: "+(string)adr+
           "\nSpread: "+(string)MarketInfo(Symbol(),MODE_SPREAD)+"; ("+(string)NormalizeDouble(100*MarketInfo(Symbol(),MODE_SPREAD)/adr,2)+"% ADR); "+
           "Avg: "+(string)getAvgSpread(NormalizeDouble(MarketInfo(Symbol(),MODE_SPREAD),2))+" (1024 tick); "+
           //"Max/Min: "+(string)ArrayMaximum(Spread,WHOLE_ARRAY,0)+"/"+
           "\n"+
           "\n[Line Area: "+getLineArea()+"]"+
           "\n"
           //"\nBuy - Lots: "+string(hitungLot(inputRisk,stoplossBuy))+", SL: "+string(stoplossBuy*P)+", Risk: "+(string)NormalizeDouble(getAmountByPips(stoplossBuy,buyLot,P),2)+" "+AccountCurrency()+
           //"\nSell - Lots: "+string(hitungLot(inputRisk,stoplossSell))+", SL: "+string(stoplossSell*P)+", Risk: "+(string)NormalizeDouble(getAmountByPips(stoplossSell,sellLot,P),2)+" "+AccountCurrency()+
           "\nEA Mode: "+EnumToString(inputEA_MODE)+" + Extension"+

           "\nBuy :: Order: "+string(getSpecifiedTotalsOrders(OP_BUY,expert_id))+"; Profit: "+(string)getOrderProfitTotal(OP_BUY,expert_id)+"; Swap: $"+doubleToStrKurung(swaplong)+"/lot"
           "\nSell :: Order: "+string(getSpecifiedTotalsOrders(OP_SELL,expert_id))+"; Profit: "+(string)getOrderProfitTotal(OP_SELL,expert_id)+"; Swap: $"+doubleToStrKurung(swapshort)+"/lot"
           "\n"+printTotalProfit(Symbol(),expert_id)+" "+printProfitBalanceRatio()
           //"\nBuy BEP Price: "+(string)getBreakEvenPrice(Symbol(),OP_BUY,OrderOpenPrice(),expert_id,P)+
           //"\nSell BEP Price: "+(string)getBreakEvenPrice(Symbol(),OP_SELL,OrderOpenPrice(),expert_id,P)
           );
  }
//+------------------------------------------------------------------+
//| Tester function                                                  |
//+------------------------------------------------------------------+
double OnTester()
  {
   double ret=0.0;
   return(ret);
  }
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam)
  {
//---Button Click

   if(id==CHARTEVENT_OBJECT_CLICK && sparam=="btnBuy" && ObjectGetInteger(0,"btnBuy",OBJPROP_STATE)==true)
     {
      Comment("Opening Buy Order...");
      if(opBuy()>0)
        {
         Sleep(1000);
         ObjectSetInteger(0,"btnBuy",OBJPROP_STATE,false);//set to not pushed
         return;
        }
      Comment("OP_BUY WITH BUTTON FAILED");
      ObjectSetInteger(0,"btnBuy",OBJPROP_STATE,false);
     }
   if(id==CHARTEVENT_OBJECT_CLICK && sparam=="btnSell" && ObjectGetInteger(0,"btnSell",OBJPROP_STATE)==true)
     {
      Comment("Opening Sell Order...");
      if(opSell()>0)
        {
         Sleep(1000);
         ObjectSetInteger(0,"btnBuy",OBJPROP_STATE,false);
         return;
        }
      Comment("OP_SELL WITH BUTTON FAILED");
      ObjectSetInteger(0,"btnBuy",OBJPROP_STATE,false);
     }
//---

   if(id==CHARTEVENT_OBJECT_DRAG || id==CHARTEVENT_OBJECT_CLICK || id==CHARTEVENT_OBJECT_CREATE || id==CHARTEVENT_CLICK)
     {
      //--macd
      macdMain=iMACD(Symbol(),PERIOD_M5,12,26,9,PRICE_CLOSE,MODE_MAIN,0);
      macdSignal=iMACD(Symbol(),PERIOD_M5,12,26,9,PRICE_CLOSE,MODE_SIGNAL,0);
      //--range pla/plv/trailing/ts
      atr=NormalizeDouble(iATR(Symbol(),PERIOD_CURRENT,14,0)/Point,2);
      adr=NormalizeDouble(iATR(Symbol(),PERIOD_D1,MathMin(64,iBars(Symbol(),PERIOD_D1)),1)/Point,2);
      pla= 5 * atr; plv= 0.1*pla; tp=0; sl=0;
      //--lINE prices
      buy_sl=getLinePrice("Buy SL");
      sell_sl=getLinePrice("Sell SL");
      stoplossBuy=getSLPips(Bid,buy_sl,P);
      stoplossSell=getSLPips(Ask,sell_sl,P);
      //--order lots
      buyLot=NormalizeDouble(hitungLot(inputRisk,stoplossBuy),2);
      sellLot=NormalizeDouble(hitungLot(inputRisk,stoplossSell),2);
      //--
      //find recognized objects
      for(int i=0;i<ArraySize(recognizedLines);i++)
        {
         if(ObjectFind(0,recognizedLines[i])>=0)
           {
            SetLineStyle(recognizedLines[i]);
            setRiskInfo(inputRisk);
            //if(autoDrawSLTP>0) drawSLTP(recognizedLines[i],autoAdjustSLTP);
           }
        }
      //--update button info
      ButtonTextChange(0,"btnBuy","Buy "+string(buyLot));
      ButtonTextChange(0,"btnSell","Sell "+string(sellLot));
      //--
      if(averagingMode!=NO_AVERAGING && inputEA_MODE==SEMI_AUTO)showAveragingInfo();
      if(averagingMode==NO_AVERAGING && inputEA_MODE==SEMI_AUTO)showSemiAutoInfo();
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void createErrorSignObj(string cmd)
  {
   if(cmd=="Buy")
     {
      string name="Buy error "+string(TimeCurrent());
      string msg="Error code = "+string(GetLastError());
      ObjectCreate(0,name,OBJ_ARROW,0,0,0,0,0);          // Create an arrow
      ObjectSetInteger(0,name,OBJPROP_ARROWCODE,174);    // Set the arrow code
      ObjectSetInteger(0,name,OBJPROP_TIME,TimeCurrent());        // Set time
      ObjectSetInteger(0,name,OBJPROP_COLOR,clrBlueViolet);        // Set color
      ObjectSetDouble(0,name,OBJPROP_PRICE,Ask);// Set price
      ObjectSetString(0,name,OBJPROP_TEXT,msg);
      Print(__FUNCTION__,
            ": Error code = ",GetLastError());
     }
   if(cmd=="Sell")
     {
      string name="Sell error "+string(TimeCurrent());
      string msg="Error code = "+string(GetLastError());
      ObjectCreate(0,name,OBJ_ARROW,0,0,0,0,0);          // Create an arrow
      ObjectSetInteger(0,name,OBJPROP_ARROWCODE,174);    // Set the arrow code
      ObjectSetInteger(0,name,OBJPROP_TIME,TimeCurrent());        // Set time
      ObjectSetInteger(0,name,OBJPROP_COLOR,clrRed);        // Set color
      ObjectSetDouble(0,name,OBJPROP_PRICE,Bid);// Set price
      ObjectSetString(0,name,OBJPROP_TEXT,msg);
      Print(__FUNCTION__,
            ": Error code = ",GetLastError());
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

void checkObjectArrowOrders()
  {
   for(int x=0;x<ObjectsTotal();x++)
     {
      string n=ObjectName(x);
      if(ObjectGet(n,OBJPROP_ARROWCODE)==241)//Arrow Up (Buy)
        {
         ObjectDelete(n);
         opBuy();
        }
      if(ObjectGet(n,OBJPROP_ARROWCODE)==242)//Arrow Down (Sell)
        {
         ObjectDelete(n);
         opSell();
        }
      if(ObjectGet(n,OBJPROP_ARROWCODE)==251) //Close All (x)
        {
         ObjectDelete(n);
         closeAllOP(expert_id,Symbol());
        }
      if(ObjectGet(n,OBJPROP_ARROWCODE)==67) //Close All Buy Thumb Up 
        {
         ObjectDelete(n);
         closeAllBuyOP();
        }
      if(ObjectGet(n,OBJPROP_ARROWCODE)==68) //close All Sell Thumb Down 
        {
         ObjectDelete(n);
         closeAllSellOP();
        }
      if(ObjectGet(n,OBJPROP_ARROWCODE)==252) //close All Sell checklist
        {
         ObjectDelete(n);
         closeAllProfit();
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int opBuy()
  {
   int ticket=OrderSend(Symbol(),OP_BUY,buyLot,Ask,10,0,0,inputComment,expert_id,0,clrBlue);
   if(ticket<0)
     {
      createErrorSignObj("Buy");
      return -1;
     }
   redrawBreakEven(OP_BUY);
   return ticket;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int opSell()
  {
   int ticket=OrderSend(Symbol(),OP_SELL,buyLot,Bid,10,0,0,inputComment,expert_id,0,clrRed);
   if(ticket<0)
     {
      createErrorSignObj("Sell");
      return -1;
     }
   redrawBreakEven(OP_SELL);
   return ticket;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int closeAllOP(int magicNum,string symbol)
  {
   for(int i=OrdersTotal()-1;i>=0;i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES))
        {
         if(OrderMagicNumber()==magicNum && OrderSymbol()==symbol)
           {
            if(OrderType()==OP_BUY && !OrderClose(OrderTicket(),OrderLots(),Bid,10,clrRed))
              {
               createErrorSignObj("Buy");
               return -1;
              }
           }
         if(OrderMagicNumber()==magicNum && OrderSymbol()==symbol)
           {
            if(OrderType()==OP_SELL && !OrderClose(OrderTicket(),OrderLots(),Ask,10,clrBlue))
              {
               createErrorSignObj("Sell");
               return -1;
              }
           }
        }
     }
   ObjectDelete("Buy Average");ObjectDelete("Sell Average");
   deleteallbuypendingorders(expert_id);   deleteallsellpendingorders(expert_id);
   return 1;
  }
//+------------------------------------------------------------------+
//|                                                              |
//+------------------------------------------------------------------+
int closeAllProfit()
  {
   for(int i=OrdersTotal()-1;i>=0;i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES))
        {
         if(OrderType()==OP_BUY && OrderProfit()>0 && OrderMagicNumber()==expert_id && OrderSymbol()==Symbol()
            && !OrderClose(OrderTicket(),OrderLots(),Bid,10,clrRed))
           {
            createErrorSignObj("Buy");
            return -1;
           }
         if(OrderType()==OP_SELL && OrderProfit()>0 && OrderMagicNumber()==expert_id && OrderSymbol()==Symbol()
            && !OrderClose(OrderTicket(),OrderLots(),Ask,10,clrBlue))
           {
            createErrorSignObj("Sell");
            return -1;
           }
        }
     }
   redrawBreakEven(OP_SELL);
   redrawBreakEven(OP_BUY);
   return 1;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int closeAllBuyOP()
  {
   if(getSpecifiedTotalsOrders(OP_BUY,expert_id)<1)return -1;
   for(int i=OrdersTotal()-1;i>=0;i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES) && OrderType()==OP_BUY)
         if(OrderMagicNumber()==expert_id && OrderSymbol()==Symbol())
           {
            if(!OrderClose(OrderTicket(),OrderLots(),Bid,10,clrRed))
              {
               createErrorSignObj("Buy");
               return -1;
              }
           }
     }
   clearChartistLines(OP_BUY);
   deleteallbuypendingorders(expert_id);
   return 1;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int closeAllBuyProfit()
  {
   for(int i=OrdersTotal()-1;i>=0;i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES) && OrderType()==OP_BUY && OrderProfit()>0)
         if(OrderMagicNumber()==expert_id && OrderSymbol()==Symbol())
           {
            if(!OrderClose(OrderTicket(),OrderLots(),Bid,10,clrRed))
              {
               createErrorSignObj("Buy");
               return -1;
              }
           }
     }
   redrawBreakEven(OP_BUY);
   return 1;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int closeAllSellOP()
  {
   if(getSpecifiedTotalsOrders(OP_SELL,expert_id)<1)return -1;
   for(int i=OrdersTotal()-1;i>=0;i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES) && OrderType()==OP_SELL)
         if(OrderMagicNumber()==expert_id && OrderSymbol()==Symbol())
           {
            if(!OrderClose(OrderTicket(),OrderLots(),Ask,10,clrBlue))
              {
               createErrorSignObj("Sell");
               return -1;
              }
           }
     }
   clearChartistLines(OP_BUY);
   deleteallsellpendingorders(expert_id);
   return 1;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int closeAllSellProfit()
  {
   for(int i=OrdersTotal()-1;i>=0;i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES) && OrderType()==OP_SELL && OrderProfit()>0)
         if(OrderMagicNumber()==expert_id && OrderSymbol()==Symbol())
           {
            if(!OrderClose(OrderTicket(),OrderLots(),Ask,10,clrBlue))
              {
               createErrorSignObj("Sell");
               return -1;
              }
           }
     }
   redrawBreakEven(OP_SELL);
   return 1;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void setRiskInfo(string Risk)
  {
   if(ObjectFind("Buy SL Text1")>=0)
     {
      string lineTextInfo="Buy SL Text1";
      double amount=getAmountByPips(stoplossBuy,buyLot);
      if(isYenPair)
        {
         amount=getAmountByPips(stoplossBuy,buyLot);
        }
      ObjectSetString(0,lineTextInfo,OBJPROP_TEXT,"Buy SL "+string(NormalizeDouble(amount,2))+" "+AccountCurrency()+" ("+(string)getEquityRasioByAmount(amount)+"%)");
      ObjectSetString(0,lineTextInfo,OBJPROP_FONT,"tahoma");
      ObjectSetInteger(0,lineTextInfo,OBJPROP_FONTSIZE,8);
     }
   if(ObjectFind("Sell SL Text1")>=0)
     {
      string lineTextInfo="Sell SL Text1";
      double amount=getAmountByPips(stoplossSell,sellLot);
      if(isYenPair)
        {
         amount=getAmountByPips(stoplossBuy,buyLot);
        }
      ObjectSetString(0,lineTextInfo,OBJPROP_TEXT,"Sell SL "+string(amount)+" "+AccountCurrency()+" ("+(string)getEquityRasioByAmount(amount)+"%)");
      ObjectSetString(0,lineTextInfo,OBJPROP_FONT,"tahoma");
      ObjectSetInteger(0,lineTextInfo,OBJPROP_FONTSIZE,8);
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
TREND readTrend(ENUM_TIMEFRAMES time_frame,int shiftBar,int toBars)
  {
   double openPrice=iOpen(Symbol(),time_frame,toBars);
   double closedPrice=iClose(Symbol(),time_frame,shiftBar);
   if(openPrice<closedPrice)
     {
      return UP;
     }
   return DOWN;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void clearFibo(string lineName)
  {
   if(lineName=="Fibo Buy")
     {
      ObjectDelete("Fibo Buy_OP");
      ObjectDelete("Fibo Buy_SL");
      ObjectDelete("Fibo Buy_TP");
     }
   if(lineName=="Fibo Sell")
     {
      ObjectDelete("Fibo Sell_OP");
      ObjectDelete("Fibo Sell_SL");
      ObjectDelete("Fibo Sell_TP");
     }
  }

#define BUY_OP 1
#define BUY_SL 2
#define BUY_TP 3
#define SELL_OP 4
#define SELL_SL 5
#define SELL_TP 6
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int isChartistLineTouched(string lineName,double bias,double lot)
  {
   bias=bias*Point*P; //bias dalam pips
   double linePrice=getLinePrice(lineName);
//cek sentuhan
   if(linePrice>0)
     {
      if(lineName=="Buy" && linePrice-bias<=Ask && Ask<=linePrice+bias)
        {
         if(allowBuy && opBuy())
           {
            ObjectDelete(lineName);
            if(!allowOppositeOrder){clearChartistLines(OP_SELL);}
            return BUY_OP;
           }
        }
      if(lineName=="Buy SL" && Bid<=linePrice+bias)
        {
         if(closeAllBuyOP()==1)
           {
            clearChartistLines(OP_BUY);
            return BUY_SL;
           }
        }
      if(lineName=="Buy TP" && Bid>=linePrice-bias)
        {
         if(closeAllBuyOP()==1)
           {
            clearChartistLines(OP_BUY);
            deleteallbuypendingorders(expert_id);
            return BUY_TP;
           }
        }
      if(lineName=="Sell" && linePrice-bias<=Bid && Bid<=linePrice+bias && allowSell && opSell())
        {
         if(!allowOppositeOrder){clearChartistLines(OP_BUY);}
         ObjectDelete(lineName);
         return SELL_OP;
        }
      if(lineName=="Sell SL" && Ask>=linePrice)
        {
         if(closeAllSellOP()==1)
           {
            clearChartistLines(OP_SELL);
            deleteallsellpendingorders(expert_id);
            return SELL_SL;
           }
        }
      if(lineName=="Sell TP" && Ask<=linePrice)
        {
         if(closeAllSellOP()==1)
           {
            clearChartistLines(OP_SELL);
            deleteallsellpendingorders(expert_id);
            return SELL_TP;
           }
        }
     }
   return 0;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int checkAveragingLine()
  {
   double bias=3*Point*P;//pips
   for(int i=1;i<=maxOrders;i++)
     {
      double averagingLine=getLinePrice("Buy"+string(i));
      if(averagingLine>=0 && getSpecifiedTotalsOrders(OP_BUY,expert_id)==i)
        {
         if(averagingLine-bias<=Ask && Ask<=averagingLine+bias)
           {
            if(opBuy())
              {
               ObjectDelete("Buy"+string(i));
               return BUY_OP;
              }
           }
        }
      averagingLine=getLinePrice("Sell"+string(i));
      if(averagingLine>=0 && getSpecifiedTotalsOrders(OP_SELL,expert_id)==i)
        {
         if(averagingLine-bias<=Ask && Ask<=averagingLine+bias)
           {
            if(opSell())
              {
               ObjectDelete("Sell"+string(i));
               return SELL_OP;
              }
           }
        }
     }
   return 0;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void deleteAvgLines(string linePrefix)
  {
   for(int i=0;i<11;i++)
     {
      if(linePrefix=="Buy")ObjectDelete("Buy"+string(i));
      if(linePrefix=="Sell")ObjectDelete("Sell"+string(i));
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void drawSLTP(string lineName,bool autoAdjust)
  {
   if(lineName=="Buy" && ObjectType(lineName)==OBJ_TREND)
     {
      double y1=ObjectGetDouble(0,lineName,OBJPROP_PRICE1);
      datetime x1=ObjectGetTimeByValue(0,lineName,y1);
      double y2=ObjectGetDouble(0,lineName,OBJPROP_PRICE2);
      datetime x2=ObjectGetTimeByValue(0,lineName,y2);
      //--
      int x1_shift = iBarShift(Symbol(),PERIOD_CURRENT,x1);
      int x2_shift = iBarShift(Symbol(),PERIOD_CURRENT,x2);
      //--
      int highest_shift=iHighest(Symbol(),PERIOD_CURRENT,MODE_HIGH,x1_shift,x2_shift);
      double highestPrice= iHigh(Symbol(),PERIOD_CURRENT,highest_shift);
      datetime high_time = iTime(Symbol(),PERIOD_CURRENT,highest_shift);
      double lineValue=ObjectGetValueByTime(0,lineName,high_time);
      //--
      double distance=highestPrice-lineValue;
      double y1_tp = y1 + (distance/2);
      double y2_tp = y2 + (distance/2);
      double y1_sl = y1 - (distance/2);
      double y2_sl = y2 - (distance/2);
      //--
      double tpLinePrice=ObjectGetValueByTime(0,"Buy TP",TimeCurrent());
      if(tpLinePrice!=y2_tp)
        {
         if(autoAdjust)
           {
            ObjectDelete("Buy TP");
            ObjectDelete("Buy SL");
           }
         ObjectCreate("Buy TP",OBJ_TREND,0,x1,y1_tp,x2,y2_tp);
         ObjectCreate("Buy SL",OBJ_TREND,0,x1,y1_sl,x2,y2_sl);
        }
     }
//--
   else if(lineName=="Sell" && ObjectType(lineName)==OBJ_TREND)
     {
      double y1=ObjectGetDouble(0,lineName,OBJPROP_PRICE1);
      datetime x1=ObjectGetTimeByValue(0,lineName,y1);
      double y2=ObjectGetDouble(0,lineName,OBJPROP_PRICE2);
      datetime x2=ObjectGetTimeByValue(0,lineName,y2);
      //--
      int x1_shift = iBarShift(Symbol(),PERIOD_CURRENT,x1);
      int x2_shift = iBarShift(Symbol(),PERIOD_CURRENT,x2);
      int lowestt_shift=iLowest(Symbol(),PERIOD_CURRENT,MODE_HIGH,x1_shift,x2_shift);
      double lowestPrice= iLow(Symbol(),PERIOD_CURRENT,lowestt_shift);
      datetime low_time = iTime(Symbol(),PERIOD_CURRENT,lowestt_shift);
      double lineValue=ObjectGetValueByTime(0,lineName,low_time);
      //--
      double distance=lineValue-lowestPrice;
      double y1_tp = y1 + (distance/2);
      double y2_tp = y2 + (distance/2);
      double y1_sl = y1 - (distance/2);
      double y2_sl = y2 - (distance/2);
      //--
      double tpLinePrice=ObjectGetValueByTime(0,"Sell TP",TimeCurrent());
      if(tpLinePrice!=y2_tp)
        {
         if(autoAdjust)
           {
            ObjectDelete("Sell TP");
            ObjectDelete("Sell SL");
           }
         ObjectCreate("Sell SL",OBJ_TREND,0,x1,y1_tp,x2,y2_tp);
         ObjectCreate("Sell TP",OBJ_TREND,0,x1,y1_sl,x2,y2_sl);
        }
     }
//--
   else if(lineName=="Buy" && ObjectType(lineName)==OBJ_HLINE)
     {
      double y=ObjectGetDouble(0,lineName,OBJPROP_PRICE);
      int highest_shift=iHighest(Symbol(),PERIOD_CURRENT,MODE_HIGH,200,0);
      double y1=iHigh(Symbol(),PERIOD_CURRENT,highest_shift);

      double distance=y1-y;
      double y1_tp = y + (distance/2);
      double y1_sl = y - (distance/2);

      double tpLinePrice=ObjectGetDouble(0,lineName,OBJPROP_PRICE);
      if(tpLinePrice!=y1_tp)
        {
         if(autoAdjust)
           {
            ObjectDelete("Buy TP");
            ObjectDelete("Buy SL");
           }
         ObjectCreate(0,"Buy TP",OBJ_HLINE,0,TimeCurrent(),y1_tp);
         ObjectCreate(0,"Buy SL",OBJ_HLINE,0,TimeCurrent(),y1_sl);
        }
     }
//--
   else if(lineName=="Sell" && ObjectType(lineName)==OBJ_HLINE)
     {
      double y=ObjectGetDouble(0,lineName,OBJPROP_PRICE);
      int lowest_shift=iLowest(Symbol(),PERIOD_CURRENT,MODE_LOW,200,0);
      double y1=iLow(Symbol(),PERIOD_CURRENT,lowest_shift);

      double distance=y1-y;
      double y1_tp = y + (distance/2);
      double y1_sl = y - (distance/2);

      double tpLinePrice=ObjectGetDouble(0,lineName,OBJPROP_PRICE);
      if(tpLinePrice!=y1_tp)
        {
         if(autoAdjust)
           {
            ObjectDelete("Sell TP");
            ObjectDelete("Sell SL");
           }
         ObjectCreate(0,"Sell TP",OBJ_HLINE,0,TimeCurrent(),y1_tp);
         ObjectCreate(0,"Sell SL",OBJ_HLINE,0,TimeCurrent(),y1_sl);
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//For ECN Broker
int MarketOrderSend(string symbol,int cmd,double volume,double price,int slip,double stoploss,double takeprofit,string comment,int magic)
  {
   int ticket;
   ticket=OrderSend(symbol,cmd,volume,price,slip,0,0,NULL,magic);
   if(ticket<=0) Alert("OrderSend Error: ",GetLastError());
//--
   else
     {
      bool res=OrderModify(ticket,0,stoploss,takeprofit,0);
      if(!res)
        {
         Alert("OrderModify Error: ",GetLastError());
         Alert("IMPORTANT: ORDER #",ticket," HAS NO STOPLOSS AND TAKEPROFIT");
        }
     }
   return(ticket);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void drawFiboLines(double slPrice,double tpPrice,datetime x1,datetime x2)
  {
   int chart_ID=0;
   string name="Fibo Auto";
   ObjectCreate(0,name,OBJ_FIBO,0,x1,slPrice,TimeCurrent(),tpPrice);
//--- set color
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clrRed);
//--- set line style
   ObjectSetInteger(chart_ID,name,OBJPROP_STYLE,STYLE_DASHDOT);
//--- set line width
   ObjectSetInteger(chart_ID,name,OBJPROP_WIDTH,1);
//--- display in the foreground (false) or background (true)
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,false);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void fiboAutoSignal(int ticket,TREND trendDirection,string risk,double bias,
                    FIBO_RETC_LEVEL entryLevel,FIBO_RETC_LEVEL stopLossLevel,FIBO_RETC_LEVEL takeProfitLevel,
                    ENUM_TIMEFRAMES timeFrame,int shiftBars=0,int toBars=1)
  {
   double levels[8]={0.0,23.6,38.2,50.0,61.8,100.0,127.0,161.8};
   int highestShift=iHighest(Symbol(),timeFrame,MODE_HIGH,toBars,shiftBars);
   int lowestShift=iLowest(Symbol(),timeFrame,MODE_LOW,toBars,shiftBars);
   int currentHighestShift=iHighest(Symbol(),PERIOD_CURRENT,MODE_HIGH,toBars,shiftBars);
   int currentLowestShift=iLowest(Symbol(),PERIOD_CURRENT,MODE_LOW,toBars,shiftBars);
   double highestPrice=iHigh(Symbol(),timeFrame,highestShift);
   double lowestPrice= iLow(Symbol(),timeFrame,lowestShift);
   double levelEntry = levels[entryLevel];
   double levelSL = levels[stopLossLevel];
   double levelTP = levels[takeProfitLevel];
   double entryPrice=NormalizeDouble(getPriceLevel(lowestPrice,highestPrice,levelEntry,trendDirection),Digits);
   double stopLossPrice=NormalizeDouble(getPriceLevel(lowestPrice,highestPrice,levelSL,trendDirection),Digits);
   double takeProfitPrice=NormalizeDouble(getPriceLevel(lowestPrice,highestPrice,levelTP,trendDirection),Digits);
   double stoplossPips=NormalizeDouble(MathAbs((entryPrice-stopLossPrice)/Point/P),1);
   double tpPips=NormalizeDouble(MathAbs((takeProfitPrice-entryPrice)/Point/P),1);
   double calculatedVolume=NormalizeDouble(hitungLot(risk,stoplossPips),2);
   bias=bias*Point*P;
   static double profit=0;
   static int orderId=0;
//draw fibonacci retracement
   if(trendDirection==UP)
     {
      ObjectDelete(0,"Fibo Auto");
      drawFiboLines(stopLossPrice,takeProfitPrice,iTime(Symbol(),timeFrame,lowestShift),iTime(Symbol(),timeFrame,highestShift));
     }
   if(trendDirection==DOWN)
     {
      ObjectDelete(0,"Fibo Auto");
      drawFiboLines(stopLossPrice,takeProfitPrice,iTime(Symbol(),timeFrame,highestShift),iTime(Symbol(),timeFrame,lowestShift));
     }
//--
//Fibo Area
   if(trendDirection==UP && Ask<entryPrice && getSpecifiedTotalsOrders(OP_BUY,expert_id)<1)
     {
      if(allowBuy && orderFiboArea)
        {
         if(macdCross(macdMain,macdSignal)==1) //cross up
           {
            Print("Fibo MACD Cross Up");
            //orderId=MarketOrderSend(Symbol(),OP_BUY,calculatedVolume,fiboTick.ask,5,stopLossPrice,takeProfitPrice,inputComment,expert_id);
            orderId=opBuy();
            if(orderId>0)
              {
               gFiboBuyTicket=orderId;
               if(OrderModify(orderId,0,stopLossPrice,takeProfitPrice,0)){}
              }
            if(orderId>0)
              {
               kirimEmail(emailTo,eaName+" Notification",Symbol()+" Fibo Buy Inside Area"+EnumToString(fiboLevelEntry));
               //return BUY_OP;
              }
           }
        }
     }
   if(trendDirection==DOWN && Bid>entryPrice && getSpecifiedTotalsOrders(OP_SELL,expert_id)<1)
     {
      if(allowSell && orderFiboArea)
        {
         if(macdCross(macdMain,macdSignal)==2) //cross down
           {
            Print("Fibo MACD Cross Down");
            //orderId=MarketOrderSend(Symbol(),OP_SELL,calculatedVolume,fiboTick.ask,5,stopLossPrice,takeProfitPrice,inputComment,expert_id);
            orderId=opSell();
            if(orderId>0)
              {
               gFiboSellTicket=orderId;
               if(OrderModify(orderId,0,stopLossPrice,takeProfitPrice,0)){}
              }
            if(orderId>0)
              {
               kirimEmail(emailTo,eaName+" Notification",Symbol()+" Fibo Sell Inside Area"+EnumToString(fiboLevelEntry));
               //return SELL_OP;
              }
           }
        }
     }
//Fibo OP by Line
   if(trendDirection==UP && entryPrice-bias<=Ask && Ask<=entryPrice+bias
      && getSpecifiedTotalsOrders(OP_BUY,expert_id)<1)
     {
      //entry at line price
      if(allowBuy && !orderFiboArea)
        {
         orderId=opBuy();
         if(orderId>0)
           {
            if(OrderModify(orderId,0,stopLossPrice,takeProfitPrice,0)){}
           }
         //orderId=MarketOrderSend(Symbol(),OP_BUY,calculatedVolume,fiboTick.ask,5,stopLossPrice,takeProfitPrice,inputComment,expert_id);
         if(orderId>0)
           {
            kirimEmail(emailTo,eaName+" Notification","Price touch Auto Fibo Entry Buy Level "+EnumToString(fiboLevelEntry));
            //return BUY_OP;
           }
        }
     }
   if(trendDirection==DOWN && entryPrice-bias<=Bid && Bid<=entryPrice+bias
      && getSpecifiedTotalsOrders(OP_SELL,expert_id)<1)
     {
      if(allowSell && !orderFiboArea)
        {
         orderId=opSell();
         if(orderId)
           {
            if(OrderModify(orderId,0,stopLossPrice,takeProfitPrice,0)){}
           }
         //orderId=MarketOrderSend(Symbol(),OP_SELL,calculatedVolume,fiboTick.ask,5,stopLossPrice,takeProfitPrice,inputComment,expert_id);
         if(orderId>0)
           {
            kirimEmail(emailTo,eaName+" Notification","Price touch Auto Fibo Entry Sell Level "+EnumToString(fiboLevelEntry));
            //return SELL_OP;
           }
        }
     }
//Stop at Opposite Trend
   if(trendDirection==UP && getSpecifiedTotalsOrders(OP_SELL,expert_id)>0 && trendChangeStop)
     {
      if(OrderMagicNumber()==expert_id && OrderSymbol()==Symbol())
        {
         if(OrderType()==OP_SELL && closeAllSellOP())
           {
            kirimEmail(emailTo,eaName+" Notification","Order Sell Closed due to fibonacci trend changing");
            //return SELL_SL;
           }
        }
     }
   if(trendDirection==DOWN && getSpecifiedTotalsOrders(OP_BUY,expert_id)>0 && trendChangeStop)
     {
      if(OrderMagicNumber()==expert_id && OrderSymbol()==Symbol())
        {
         if(OrderType()==OP_BUY && closeAllBuyOP())
           {
            kirimEmail(emailTo,eaName+" Notification","Order Buy Closed due to fibonacci trend changing");
            //return BUY_SL;
           }
        }
     }
//profit variabels
   if(OrderSelect(orderId,SELECT_BY_TICKET,MODE_TRADES))
     {
      profit=OrderProfit();
     }
   if(OrdersTotal()<1)
     {
      profit=0;
     }
//MACD Close
   if(exitTimeframe && getOrderProfitTotal(OP_BUY,expert_id)>=getMinimumTP2USD(minimumTakeProfit)
      && macdCross(macdMain,macdSignal)==2)//cross down
     {
      int retClose=closeAllBuyOP();
      if(emailTo!="" && retClose==1)
        {
         kirimEmail(emailTo,"Reversal Signal Buy Close",Symbol()+" Buy Order closed by reversal 123123");
        }
     }
   if(exitTimeframe && getOrderProfitTotal(OP_SELL,expert_id)>=getMinimumTP2USD(minimumTakeProfit))
     {
      if(macdCross(macdMain,macdSignal)==1)//cross up
        {
         int retClose=closeAllSellOP();
         if(emailTo!="" && retClose==1)
           {
            kirimEmail(emailTo,"Reversal Signal Sell Close",Symbol()+" Sell Order closed by reversal");
           }
        }
     }
//Chart Info
   Comment(eaName+", Last Modify: "+string(__DATETIME__)+", Expert ID: "+string(expert_id)+"/"+Symbol()+" "+inputComment+
           "\n"+
           "\nLocal Time: "+TimeToStr(TimeLocal())+", GMT: "+TimeToStr(TimeGMT())+
           "\n\nLots: "+string(calculatedVolume)+", SL: "+string(stoplossPips*P)+", TP: "+string(tpPips*P)+" Risk: "+string(risk)+
           "\nATR: "+string(NormalizeDouble(iATR(Symbol(),PERIOD_CURRENT,14,0)/Point,1))+" points, Daily Range: "+string(NormalizeDouble(iATR(Symbol(),PERIOD_D1,14,0)/Point,1))+" points"+
           "\nPLA/PLV: "+(string)(2*adr)+"/"+(string)(0.2*adr)+
           "\nMinimun Profit: "+(string)getMinimumTP2USD(minimumTakeProfit)+" "+AccountCurrency()+
           "\n"
           "\nEA Mode: "+"Auto"+
           "\n"+
           "\nFibonacci Algorithm::"+" Trend: "+EnumToString(trendDirection)+
           "\nFibonacci Period: "+EnumToString(inputTimeFrame)+
           "\nStart Index: "+(string)shift+"; Last Index: "+(string)to+
           "\nEntry Level: "+EnumToString(entryLevel)+"; "+"Entry Price/Area: "+(string)entryPrice+
           "\nSL Level: "+EnumToString(stopLossLevel)+"; "+"Stop Loss Price: "+(string)stopLossPrice+
           "\nTP Level: "+EnumToString(takeProfitLevel)+"; "+"Take Profit Price: "+(string)takeProfitPrice+
           "\n"
           "\nBuy :: Order: "+string(getSpecifiedTotalsOrders(OP_BUY,expert_id))+"; Profit: $"+(string)getOrderProfitTotal(OP_BUY,expert_id)+"; Swap: $"+doubleToStrKurung(swaplong)+"/lot;"+
           //"Points:"+(string)getOrderPipsByTicket(gFiboBuyTicket,expert_id)+
           "\nSell :: Order: "+string(getSpecifiedTotalsOrders(OP_SELL,expert_id))+"; Profit: $"+(string)getOrderProfitTotal(OP_SELL,expert_id)+"; Swap: $"+doubleToStrKurung(swapshort)+"/lot;"+
           //"Points:"+(string)getOrderPipsByTicket(gFiboSellTicket,expert_id)+
           "\nTotal Profit: $"+fiboProfitAmount(profit)+" ("+fiboProfitPercent(profit)+"% From Balance)");
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string fiboProfitAmount(double orderProfit)
  {
   if(orderProfit==0)
     {
      return "N/A";
     }
   return string(orderProfit);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string fiboProfitPercent(double opProfit)
  {
   if(opProfit==0)
     {
      return "N/A";
     }
   return string(NormalizeDouble(100*opProfit/AccountBalance(),1));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getRiskAmount(string risk)
  {
   string delimiter=cariDelimiter(risk);
   ushort delimiterCode=StringGetChar(delimiter,0);
   string hasilSplit[];
   int angka=StringSplit(risk,delimiterCode,hasilSplit);
   double angkaRisiko=StrToDouble(hasilSplit[0])*1.0;
   double besarRisiko=AccountEquity()*angkaRisiko/100;
   return besarRisiko;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getMinimumTP2USD(string minTP)//USD quoted only
  {
   if(minTP=="")
     {
      return 0;
     }
   string delimiter=cariDelimiter(minTP);
   ushort delimiterCode=StringGetChar(delimiter,0);
   string hasilSplit[];
   int angka=StringSplit(minTP,delimiterCode,hasilSplit);
   double angkaTP=StrToDouble(hasilSplit[0]);
//--
   if(delimiter=="%")
     {
      return NormalizeDouble(AccountEquity()*angkaTP/100,2);
     }
//--
   else if(delimiter=="USD" || delimiter=="-1")
     {
      return angkaTP;
     }
   return -1;
  }
//+------------------------------------------------------------------+
//|Functions untuk mendapatkan lot/volume berdassarkan input risiko               
//+------------------------------------------------------------------+
double hitungLot(string risk,double stopLossPips)
  {
   double lot=0.0; //inisiasi nilai lot = 9
   string delimiter=cariDelimiter(risk); //mencari delimiter
   ushort delimiterCode=StringGetChar(delimiter,0); //menyimpan kode delimiter
   string hasilSplit[]; //deklarasi array
   int angka=StringSplit(risk,delimiterCode,hasilSplit);
   double angkaRisiko=StrToDouble(hasilSplit[0]); //menampung angka
   if(delimiter=="%" || delimiter=="%risk")
     {
      if(stopLossPips==0)
        {
         lot=AccountEquity()/(MarketInfo(Symbol(),MODE_LOTSIZE))*angkaRisiko; //lot berdasarkan ekuitas
                                                                              //lot=lot*100;
         if(isYenPair)
           {
            //lot=lot*100; //konfigurasi yen
           }
         if(lot<0.01)
           {
            return MarketInfo(Symbol(),MODE_MINLOT); //lot minimal
           }
         return NormalizeDouble(lot,2);
        }
      else
        {
         double besarRisiko=AccountEquity()*angkaRisiko/100;
         //lot=besarRisiko/(MathAbs(jarakSL)*100000);
         //lot berdasarkan jarak SL
         lot=besarRisiko/(MarketInfo(Symbol(),MODE_LOTSIZE)*stopLossPips*Point*P);
         //konfigurasi yen

         if(isYenPair)
           {
            lot=lot*100;
           }
         if(lot<0.01)
           {
            return MarketInfo(Symbol(),MODE_MINLOT); //lot minimal
           }
         return NormalizeDouble(lot,2);
        }
     }
   else if(delimiter=="lot" || delimiter=="Lot")
     {
      lot=angkaRisiko;
      return NormalizeDouble(lot,2); //fixed lot
     }
   return -1;
  }
//+------------------------------------------------------------------+
//|Functions untuk mendapatkan delimiter yg dikenali                 |
//+------------------------------------------------------------------+
string cariDelimiter(string kata)
  {
   string delimiters[]={"%","%Risk","%risk","Lot","lot","lots","Lots"};
   for(int i=0; i<ArraySize(delimiters); i++)
      //--
     {
      if(StringFind(kata,delimiters[i],1)!=-1)
        {
         return delimiters[i];
        }
     }
   return("-1");
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getPriceLevel(double lowestPrice,double highestPrice,double level,TREND trend_direction)
  {
   double priceLevel;
   double priceDistance=highestPrice-lowestPrice;
//--
   if(trend_direction==UP)
     {
      priceLevel=highestPrice -(level/100*priceDistance);
      return NormalizeDouble(priceLevel,5);
     }
//--
   else if(trend_direction==DOWN)
     {
      priceLevel=lowestPrice+(level/100*priceDistance);
      return NormalizeDouble(priceLevel,5);
     }
//--
   else
     {
      return 0;
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void setLineInfo(string textName,string lineName)
  {
//create
   if(ObjectFind(textName)<0)
     {
      if(ObjectType(lineName)==OBJ_HLINE)
        {
         ObjectCreate(0,textName,OBJ_TEXT,0,TimeCurrent(),getLinePrice(lineName));
         ObjectSetString(0,textName,OBJPROP_TEXT,lineName);
         ObjectSetString(0,textName,OBJPROP_FONT,"tahoma");
         ObjectSetInteger(0,textName,OBJPROP_FONTSIZE,8);
        }
     }
//update

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SetLineStyle(string lineName)
  {
   string lineTextInfo=lineName+" Text1";
   string lineSeparator= lineName+" Text2";
   string linePriceTag = lineName+" Text3";
// 8 dash for price separator
   datetime bartimes[8];
   double previousBarTime= CopyTime(Symbol(),PERIOD_CURRENT,0,8,bartimes);
   datetime selisihWaktu = TimeCurrent()-bartimes[0];
//line properties
   double lineCurrentPrice=NormalizeDouble(ObjectGetValueByTime(0,lineName,TimeCurrent()),5);
   datetime lineCurrentTime=ObjectGetTimeByValue(0,lineName,lineCurrentPrice);
   double linePrice1 = NormalizeDouble(ObjectGetDouble(0,lineName,OBJPROP_PRICE1),5);
   double linePrice2 = NormalizeDouble(ObjectGetDouble(0,lineName,OBJPROP_PRICE2),5);
   datetime lineTime1= ObjectGetTimeByValue(0,lineName,linePrice1);
//double lineTime2 = ObjectGet(lineName,OBJPROP_TIME2)*1.0;

//set line colors
   if(lineName=="Buy" || lineName=="Buy SL" || lineName=="Buy TP" || lineName=="Buy Upper Band" || lineName=="Buy Lower Band")
     {
      ObjectSetInteger(0,lineName,OBJPROP_COLOR,clrDodgerBlue); //set line color
     }
   else if(lineName=="Sell" || lineName=="Sell SL" || lineName=="Sell TP" || lineName=="Sell Upper Band" || lineName=="Sell Lower Band")
     {
      ObjectSetInteger(0,lineName,OBJPROP_COLOR,clrPink); //set line color
     }
//Create Text info if not exist
   if(ObjectFind(lineTextInfo)<0)
     {
      setLineInfo(lineTextInfo,lineName);//for horizontal
      if(ObjectType(lineName)==OBJ_TREND)
        {
         ObjectCreate(0,lineTextInfo,OBJ_TEXT,0,lineTime1,linePrice1);
         ObjectSetString(0,lineTextInfo,OBJPROP_TEXT,lineName);
         ObjectSetString(0,lineTextInfo,OBJPROP_FONT,"tahoma");
         ObjectSetInteger(0,lineTextInfo,OBJPROP_FONTSIZE,8);
        }
     }
   else
     { //update position if exist and moved
      if(ObjectType(lineName)==OBJ_TREND)
        {
         if(ObjectGet(lineTextInfo,OBJPROP_TIME1)!=lineTime1
            || ObjectGet(lineTextInfo,OBJPROP_PRICE1)!=linePrice1)
           {
            ObjectSetInteger(0,lineTextInfo,OBJPROP_TIME1,lineTime1);
            ObjectSetDouble(0,lineTextInfo,OBJPROP_PRICE1,linePrice1);
           }
        }
      if(ObjectType(lineName)==OBJ_HLINE)
        {
         if(ObjectGet(lineTextInfo,OBJPROP_PRICE1)!=getLinePrice(lineName))
           {
            ObjectSetInteger(0,lineTextInfo,OBJPROP_TIME1,TimeCurrent());
            ObjectSetDouble(0,lineTextInfo,OBJPROP_PRICE1,getLinePrice(lineName));
           }
        }
     }
//separator
   if(ObjectFind(lineSeparator)<0)
     {
      ObjectCreate(0,lineSeparator,OBJ_TREND,0,TimeCurrent(),lineCurrentPrice,TimeCurrent()+selisihWaktu,lineCurrentPrice);
      ObjectSetInteger(0,lineSeparator,OBJPROP_RAY_RIGHT,false);
      ObjectSetInteger(0,lineSeparator,OBJPROP_STYLE,STYLE_DOT);
      if(lineName=="Sell" || lineName=="Sell SL" || lineName=="Sell TP"
         || lineName=="Sell Upper Band" || lineName=="Sell Lower Band")
        {
         ObjectSetInteger(0,lineSeparator,OBJPROP_COLOR,clrPink);
        }
      else
        {
         ObjectSetInteger(0,lineSeparator,OBJPROP_COLOR,clrDodgerBlue);
        }
     }
   else
     {
      //update position if exist and moved
      if(ObjectGet(lineSeparator,OBJPROP_TIME1)!=TimeCurrent()
         || ObjectGet(lineSeparator,OBJPROP_PRICE1)!=lineCurrentPrice)
        {
         ObjectSetInteger(0,lineSeparator,OBJPROP_TIME1,TimeCurrent());
         ObjectSetInteger(0,lineSeparator,OBJPROP_TIME2,TimeCurrent()+selisihWaktu);
         ObjectSetDouble(0,lineSeparator,OBJPROP_PRICE1,lineCurrentPrice);
         ObjectSetDouble(0,lineSeparator,OBJPROP_PRICE2,lineCurrentPrice);
        }
     }
//Price Tag
   if(ObjectFind(linePriceTag)<0)
     {
      ObjectCreate(0,linePriceTag,OBJ_ARROW_RIGHT_PRICE,0,TimeCurrent()+selisihWaktu,lineCurrentPrice);
      if(lineName=="Sell" || lineName=="Sell SL" || lineName=="Sell TP"
         || lineName=="Sell Upper Band" || lineName=="Sell Lower Band")
        {
         ObjectSetInteger(0,linePriceTag,OBJPROP_COLOR,clrPink);
        }
      else
        {
         ObjectSetInteger(0,linePriceTag,OBJPROP_COLOR,clrDodgerBlue);
        }
      //lineBuyCurrentPrice = lineCurrentPrice;
     }
   else
     {
      //update tag position if exist and move
      if(ObjectGet(linePriceTag,OBJPROP_TIME1)!=TimeCurrent()+selisihWaktu
         || ObjectGet(linePriceTag,OBJPROP_PRICE1)!=lineCurrentPrice)
        {
         ObjectSetInteger(0,linePriceTag,OBJPROP_TIME1,TimeCurrent()+selisihWaktu);
         ObjectSetDouble(0,linePriceTag,OBJPROP_PRICE1,lineCurrentPrice);
         //lineBuyCurrentPrice = lineCurrentPrice;
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool ButtonCreate(const long              chart_ID=0,               // chart's ID
                  const string            name="Button",            // button name
                  const int               sub_window=0,             // subwindow index
                  const int               x=0,                      // X coordinate
                  const int               y=0,                      // Y coordinate
                  const int               width=50,                 // button width
                  const int               height=18,                // button height
                  const ENUM_BASE_CORNER  corner=CORNER_LEFT_UPPER, // chart corner for anchoring
                  const string            text="Button",            // text
                  const string            font="Arial",             // font
                  const int               font_size=10,             // font size
                  const color             clr=clrBlack,             // text color
                  const color             back_clr=C'236,233,216',  // background color
                  const color             border_clr=clrNONE,       // border color
                  const bool              state=false,              // pressed/released
                  const bool              back=false,               // in the background
                  const bool              selection=false,          // highlight to move
                  const bool              hidden=true,              // hidden in the object list
                  const long              z_order=0)                // priority for mouse click
  {
//--- reset the error value
   ResetLastError();
//--- create the button
   if(!ObjectCreate(chart_ID,name,OBJ_BUTTON,sub_window,0,0))
     {
      Print(__FUNCTION__,
            ": failed to create the button! Error code = ",GetLastError());
      return(false);
     }
//--- set button coordinates
   ObjectSetInteger(chart_ID,name,OBJPROP_XDISTANCE,x);
   ObjectSetInteger(chart_ID,name,OBJPROP_YDISTANCE,y);
//--- set button size
   ObjectSetInteger(chart_ID,name,OBJPROP_XSIZE,width);
   ObjectSetInteger(chart_ID,name,OBJPROP_YSIZE,height);
//--- set the chart's corner, relative to which point coordinates are defined
   ObjectSetInteger(chart_ID,name,OBJPROP_CORNER,corner);
//--- set the text
   ObjectSetString(chart_ID,name,OBJPROP_TEXT,text);
//--- set text font
   ObjectSetString(chart_ID,name,OBJPROP_FONT,font);
//--- set font size
   ObjectSetInteger(chart_ID,name,OBJPROP_FONTSIZE,font_size);
//--- set text color
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);
//--- set background color
   ObjectSetInteger(chart_ID,name,OBJPROP_BGCOLOR,back_clr);
//--- set border color
   ObjectSetInteger(chart_ID,name,OBJPROP_BORDER_COLOR,border_clr);
//--- display in the foreground (false) or background (true)
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back);
//--- set button state
   ObjectSetInteger(chart_ID,name,OBJPROP_STATE,state);
//--- enable (true) or disable (false) the mode of moving the button by mouse
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection);
//--- hide (true) or display (false) graphical object name in the object list
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden);
//--- set the priority for receiving the event of a mouse click in the chart
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order);
//--- successful execution
   return(true);
  }
//+------------------------------------------------------------------+
//| Move the button                                                  |
//+------------------------------------------------------------------+
bool ButtonMove(const long   chart_ID=0,    // chart's ID
                const string name="Button", // button name
                const int    x=0,           // X coordinate
                const int    y=0)           // Y coordinate
  {
//--- reset the error value
   ResetLastError();
//--- move the button
   if(!ObjectSetInteger(chart_ID,name,OBJPROP_XDISTANCE,x))
     {
      Print(__FUNCTION__,
            ": failed to move X coordinate of the button! Error code = ",GetLastError());
      return(false);
     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(!ObjectSetInteger(chart_ID,name,OBJPROP_YDISTANCE,y))
     {
      Print(__FUNCTION__,
            ": failed to move Y coordinate of the button! Error code = ",GetLastError());
      return(false);
     }
//--- successful execution
   return(true);
  }
//+------------------------------------------------------------------+
//| Change button size                                               |
//+------------------------------------------------------------------+
bool ButtonChangeSize(const long   chart_ID=0,    // chart's ID
                      const string name="Button", // button name
                      const int    width=50,      // button width
                      const int    height=18)     // button height
  {
//--- reset the error value
   ResetLastError();
//--- change the button size
   if(!ObjectSetInteger(chart_ID,name,OBJPROP_XSIZE,width))
     {
      Print(__FUNCTION__,
            ": failed to change the button width! Error code = ",GetLastError());
      return(false);
     }
//--
   if(!ObjectSetInteger(chart_ID,name,OBJPROP_YSIZE,height))
     {
      Print(__FUNCTION__,
            ": failed to change the button height! Error code = ",GetLastError());
      return(false);
     }
//--- successful execution
   return(true);
  }
//+------------------------------------------------------------------+
//| Change corner of the chart for binding the button                |
//+------------------------------------------------------------------+
bool ButtonChangeCorner(const long             chart_ID=0,               // chart's ID
                        const string           name="Button",            // button name
                        const ENUM_BASE_CORNER corner=CORNER_LEFT_UPPER) // chart corner for anchoring
  {
//--- reset the error value
   ResetLastError();
//--- change anchor corner
   if(!ObjectSetInteger(chart_ID,name,OBJPROP_CORNER,corner))
     {
      Print(__FUNCTION__,
            ": failed to change the anchor corner! Error code = ",GetLastError());
      return(false);
     }
//--- successful execution
   return(true);
  }
//+------------------------------------------------------------------+
//| Change button text                                               |
//+------------------------------------------------------------------+
bool ButtonTextChange(const long   chart_ID=0,    // chart's ID
                      const string name="Button", // button name
                      const string text="Text")   // text
  {
//--- reset the error value
   ResetLastError();
//--- change object text
   if(!ObjectSetString(chart_ID,name,OBJPROP_TEXT,text))
     {
      Print(__FUNCTION__,
            ": failed to change the text! Error code = ",GetLastError());
      return(false);
     }
//--- successful execution
   return(true);
  }
//+------------------------------------------------------------------+
//| Delete the button                                                |
//+------------------------------------------------------------------+
bool ButtonDelete(const long   chart_ID=0,    // chart's ID
                  const string name="Button") // button name
  {
//--- reset the error value
   ResetLastError();
//--- delete the button
   if(!ObjectDelete(chart_ID,name))
     {
      Print(__FUNCTION__,
            ": failed to delete the button! Error code = ",GetLastError());
      return(false);
     }
//--- successful execution
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool login()
  {
   Comment("Login to 185.151.28.154 ....");
   string webs=""+website+"?acc="+IntegerToString(AccountNumber())+"&id="+EA_ID;
   static bool LOGEDIN=0;
   if(LOGEDIN)return(1);
   string ret=httpGET(webs); //send get request
   if(StringFind(ret,"Success",0)>=0)LOGEDIN=1;
   Comment(ret);
   if(openwebif_fail && !LOGEDIN){httpOpen(fail_website);openwebif_fail=0;}
   if(!LOGEDIN)Sleep(5000);
   return(LOGEDIN);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool kirimEmail(string toEmail,string subj,string msg)
  {
   string webs=""+webmail+"?act=sendemail&to="+toEmail+"&sbj="+subj+"&msg="+msg;
//Print(webs);
   string ret=httpGET(webs); //send get request
   if(StringFind(ret,"success",0)>=0)
     {
      Print(ret);
      Print(" Email Sent");
      return true;
     }
   Print(" Email Not Sent");
   return false;
  }

#import "shell32.dll"
int ShellExecuteW(
                  int hwnd,
                  string Operation,
                  string File,
                  string Parameters,
                  string Directory,
                  int ShowCmd
                  );

#import "wininet.dll"

int InternetOpenW(
                  string     sAgent,
                  int        lAccessType,
                  string     sProxyName="",
                  string     sProxyBypass="",
                  int     lFlags=0
                  );

int InternetOpenUrlW(
                     int     hInternetSession,
                     string     sUrl,
                     string     sHeaders="",
                     int     lHeadersLength=0,
                     uint     lFlags=0,
                     int     lContext=0
                     );

int InternetReadFile(
                     int     hFile,
                     uchar     &sBuffer[],
                     int     lNumBytesToRead,
                     int     &lNumberOfBytesRead
                     );

int InternetCloseHandle(
                        int     hInet
                        );
#import

#define INTERNET_FLAG_RELOAD            0x80000000
#define INTERNET_FLAG_NO_CACHE_WRITE    0x04000000
#define INTERNET_FLAG_PRAGMA_NOCACHE    0x00000100

int hSession_IEType;
int hSession_Direct;
int Internet_Open_Type_Preconfig=0;
int Internet_Open_Type_Direct=1;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int hSession(bool Direct)
  {
   string InternetAgent="Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; Q312461)";
//-
   if(Direct)
     {
      if(hSession_Direct==0)
        {
         hSession_Direct=InternetOpenW(InternetAgent,Internet_Open_Type_Direct,"0","0",0);
        }

      return(hSession_Direct);
     }
//--
   else
     {
      if(hSession_IEType==0)
        {
         hSession_IEType=InternetOpenW(InternetAgent,Internet_Open_Type_Preconfig,"0","0",0);
        }

      return(hSession_IEType);
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string httpGET(string strUrl)
  {
   int handler=hSession(false);
   int response=InternetOpenUrlW(handler,strUrl,NULL,0,
                                 INTERNET_FLAG_NO_CACHE_WRITE|
                                 INTERNET_FLAG_PRAGMA_NOCACHE|
                                 INTERNET_FLAG_RELOAD,0);
   if(response==0)
      return "false";

   uchar ch[100]; string toStr=""; int dwBytes,h=-1;
//--
   while(InternetReadFile(response,ch,100,dwBytes))
     {
      if(dwBytes<=0) break; toStr=toStr+CharArrayToString(ch,0,dwBytes);
     }

   InternetCloseHandle(response);
   return toStr;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void httpOpen(string strUrl)
  {
   Shell32::ShellExecuteW(0,"open",strUrl,"","",3);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string PrintTimeframe(int _a)
  {
   if(_a==0)
      _a=Period();

   string s;
   switch(_a)
     {
      case 1: s="M1"; break;
      case 5: s="M5"; break;
      case 15: s="M15"; break;
      case 30: s="M30"; break;
      case 60: s="H1"; break;
      case 240: s="H4"; break;
      case 1440: s="D1"; break;
      case 10080: s="W1"; break;
      case 43200: s="MN1"; break;
      default:
         s="unknown";
     }

   return(s);
  }
//+------------------------------------------------------------------+
